# coding: utf-8

import os
import time
import importlib
import json
from collections import OrderedDict
import logging
import argparse
import numpy as np
import random
import imageio 

import torch
import torch.nn as nn
import torch.optim
import torch.utils.data
import torch.backends.cudnn
import torchvision.utils
from torchvision import utils
try:
    from tensorboardX import SummaryWriter
    is_tensorboard_available = True
except Exception:
    is_tensorboard_available = False

from dataloader import get_train_val
from dataloader import AglLoader
import torch.utils.data as data

torch.backends.cudnn.benchmark = True

print(is_tensorboard_available)
logging.basicConfig(
    format='[%(asctime)s %(name)s %(levelname)s] - %(message)s',
    datefmt='%Y/%m/%d %H:%M:%S',
    level=logging.DEBUG)
logger = logging.getLogger(__name__)

global_step = 0


def str2bool(s):
    if s.lower() == 'true':
        return True
    elif s.lower() == 'false':
        return False
    else:
        raise RuntimeError('Boolean value expected')


def parse_args():
    parser = argparse.ArgumentParser()
    # model config
    parser.add_argument('--block_type', type=str, default="basic")
    parser.add_argument('--depth', type=int, default="50")
    parser.add_argument('--base_channels', type=int, default=16)

    # run config
    parser.add_argument('--outdir', type=str, required=True)
    parser.add_argument('--seed', type=int, default=17)
    parser.add_argument('--num_workers', type=int, default=7)

    # optim config
    parser.add_argument('--epochs', type=int, default=1000)
    parser.add_argument('--batch_size', type=int, default=32)
    parser.add_argument('--base_lr', type=float, default=0.1)
    parser.add_argument('--weight_decay', type=float, default=1e-4)
    parser.add_argument('--momentum', type=float, default=0.9)
    parser.add_argument('--nesterov', type=str2bool, default=True)
    parser.add_argument('--milestones', type=str, default='[80, 120]')
    parser.add_argument('--lr_decay', type=float, default=0.1)

    # TensorBoard
    parser.add_argument('--tensorboard', dest='tensorboard', default='true')

    args = parser.parse_args()
    if not is_tensorboard_available:
        args.tensorboard = False

    model_config = OrderedDict([
        ('arch', 'resnet'),
        ('block_type', args.block_type),
        ('depth', args.depth),
        ('base_channels', args.base_channels),
        ('input_shape', (1, 3, 112, 112)),
        ('n_classes', 2),
    ])

    optim_config = OrderedDict([
        ('epochs', args.epochs),
        ('batch_size', args.batch_size),
        ('base_lr', args.base_lr),
        ('weight_decay', args.weight_decay),
        ('momentum', args.momentum),
        ('nesterov', args.nesterov),
        ('milestones', json.loads(args.milestones)),
        ('lr_decay', args.lr_decay),
    ])

    run_config = OrderedDict([
        ('seed', args.seed),
        ('outdir', args.outdir),
        ('num_workers', args.num_workers),
        ('tensorboard', args.tensorboard),
    ])

    config = OrderedDict([
        ('model_config', model_config),
        ('optim_config', optim_config),
        ('run_config', run_config),
    ])

    return config


def load_model(config):
    module = importlib.import_module(config['arch'])
    Network = getattr(module, 'Network')
    return Network(config)

def show_batch(sample_batched):
    #sample_batched = iter(sample_batched).next()
    # Gridify only two images and masks
    grid = utils.make_grid(sample_batched, padding=2, nrow=1)
    # Is network is given then concat also prediction
    imageio.imsave("test_batch.jpg", grid.numpy().transpose((1, 2, 0)))

class AverageMeter(object):
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, num):
        self.val = val
        self.sum += val * num
        self.count += num
        self.avg = self.sum / self.count


def train(epoch, model, optimizer, criterion, train_loader, run_config,
          writer):
    global global_step
    print('++ Train {}'.format(epoch))
    model.train()

    loss_meter = AverageMeter()
    accuracy_meter = AverageMeter()

    for step, (data, targets) in enumerate(train_loader):
        global_step += 1

        #if run_config['tensorboard'] and step % 10 == 0:
        #    image = torchvision.utils.make_grid(
        #        data, normalize=True, scale_each=True)
        #    writer.add_image('Train/Image', image, epoch)

        data = data.cuda()
        targets = targets.cuda()

        optimizer.zero_grad()

        outputs = model(data)
        loss = criterion(outputs, targets)
        loss.backward()

        optimizer.step()

        _, preds = torch.max(outputs, dim=1)

        loss_ = loss.item()
        correct_ = preds.eq(targets).sum().item()
        num = data.size(0)

        accuracy = correct_ / num

        loss_meter.update(loss_, num)
        accuracy_meter.update(accuracy, num)

        #if run_config['tensorboard']:
        #    writer.add_scalar('Train/RunningLoss', loss_, global_step)
        #    writer.add_scalar('Train/RunningAccuracy', accuracy, global_step)

    print('=========== Epoch {} '
                'Loss {:.4f} '
                'Accuracy {:.4f}'.format(
                    epoch,
                    loss_meter.avg,
                    accuracy_meter.avg,
                ))

    if run_config['tensorboard']:
        writer.add_scalar('Train/Loss', loss_meter.avg, epoch)
        writer.add_scalar('Train/Accuracy', accuracy_meter.avg, epoch)
        #writer.add_scalar('Train/Time', elapsed, epoch)


def test(epoch, model, criterion, test_loader, run_config, writer):
    print('+= Test {}'.format(epoch))

    model.eval()

    loss_meter = AverageMeter()
    correct_meter = AverageMeter()
    start = time.time()
    for step, (data, targets) in enumerate(test_loader):

        #if run_config['tensorboard'] and epoch == 0 and step == 0:
        #    image = torchvision.utils.make_grid(
        #        data, normalize=True, scale_each=True)
        #    writer.add_image('Test/Image', image, epoch)

        data = data.cuda()
        targets = targets.cuda()
        with torch.no_grad():
            outputs = model(data)
        loss = criterion(outputs, targets)

        _, preds = torch.max(outputs, dim=1)
        #if (step % 10 == 0):
        #    print(preds)
        #    print(targets)
        #    print("=====")

        loss_ = loss.item()
        correct_ = preds.eq(targets).sum().item()
        num = data.size(0)

        loss_meter.update(loss_, num)
        correct_meter.update(correct_, 1)

    accuracy = correct_meter.sum / len(test_loader.dataset)

    print('=========== Epoch {} Loss {:.4f} Accuracy {:.4f}'.format(
        epoch, loss_meter.avg, accuracy))

    #elapsed = time.time() - start
    #logger.info('Elapsed {:.2f}'.format(elapsed))

    if run_config['tensorboard']:
        writer.add_scalar('Test/Accuracy', accuracy, epoch)
        writer.add_scalar('Test/Loss', loss_meter.avg, epoch)
        #writer.add_scalar('Test/Time', elapsed, epoch)

        #for name, param in model.named_parameters():
        #    writer.add_histogram(name, param, global_step)

    return accuracy


def main():
    # parse command line arguments
    config = parse_args()
    logger.info(json.dumps(config, indent=2))

    run_config = config['run_config']
    optim_config = config['optim_config']

    # TensorBoard SummaryWriter
    writer = SummaryWriter() if run_config['tensorboard'] else None

    # set random seed
    seed = run_config['seed']
    torch.manual_seed(seed)
    np.random.seed(seed)
    random.seed(seed)

    # create output directory
    outdir = run_config['outdir']
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    # save config as json file in output directory
    outpath = os.path.join(outdir, 'config.json')
    with open(outpath, 'w') as fout:
        json.dump(config, fout, indent=2)

    # data loaders
    x_train, y_train, x_val, y_val = get_train_val(agl_dirs=["dataset/agl", "dataset/agl_br"],
                                                   no_agl_dirs=["dataset/not_agl", "dataset/not_agl_br"])
    _train_loader = AglLoader(x_train, y_train, val=False) 
    _test_loader = AglLoader(x_val, y_val, val=True)
    train_loader = data.DataLoader(_train_loader, batch_size=optim_config['batch_size'],
                                   num_workers=run_config['num_workers'])
    test_loader = data.DataLoader(_test_loader, batch_size=optim_config['batch_size'],
                                  num_workers=run_config['num_workers'])

    # model
    model = load_model(config['model_config'])
    model.cuda()
    n_params = sum([param.view(-1).size()[0] for param in model.parameters()])
    logger.info('n_params: {}'.format(n_params))

    criterion = nn.CrossEntropyLoss(reduction='mean')

    # optimizer
    optimizer = torch.optim.SGD(
        model.parameters(),
        lr=optim_config['base_lr'],
        weight_decay=optim_config['weight_decay'],
        momentum=optim_config['momentum'],
        nesterov=optim_config['nesterov'])
    scheduler = torch.optim.lr_scheduler.MultiStepLR(
        optimizer,
        milestones=optim_config['milestones'],
        gamma=optim_config['lr_decay'])

    # run test before start training
    test(0, model, criterion, test_loader, run_config, writer)

    for epoch in range(1, optim_config['epochs'] + 1):
        print("*************************** New epoch %d **********************" % epoch)

        train(epoch, model, optimizer, criterion, train_loader, run_config,
              writer)
        accuracy = test(epoch, model, criterion, test_loader, run_config,
                        writer)
        scheduler.step()

        state = OrderedDict([
            ('config', config),
            ('state_dict', model.state_dict()),
            ('optimizer', optimizer.state_dict()),
            ('epoch', epoch),
            ('accuracy', accuracy),
        ])
        model_path = os.path.join(outdir, 'model_state.pth')
        torch.save(state, model_path)

    if run_config['tensorboard']:
        outpath = os.path.join(outdir, 'all_scalars.json')
        writer.export_scalars_to_json(outpath)


if __name__ == '__main__':
    main()
