import os
import time
import importlib
import json
from collections import OrderedDict
import logging
import argparse
import numpy as np
import random
import imageio 
import glob
import cv2
import tqdm

import torch
import torch.nn as nn
import torch.optim
import torch.utils.data
import torch.backends.cudnn
import torchvision.utils
from torchvision import utils
import torchvision.transforms.functional as TF

from dataloader import get_train_val
from dataloader import AglLoader
import torch.utils.data as data

torch.backends.cudnn.benchmark = True

global_step = 0

def parse_args():
    parser = argparse.ArgumentParser()
    # model config
    parser.add_argument('--block_type', type=str, default="basic")
    parser.add_argument('--depth', type=int, default="50")
    parser.add_argument('--base_channels', type=int, default=16)

    # run config
    parser.add_argument('--weights', type=str, required=True)
    parser.add_argument('--seed', type=int, default=17)
    parser.add_argument('--num_workers', type=int, default=7)
    parser.add_argument('--test', type=str, required=True)
    parser.add_argument('--result', type=str, required=True)

    # TensorBoard
    args = parser.parse_args()

    model_config = OrderedDict([
        ('arch', 'resnet'),
        ('block_type', args.block_type),
        ('depth', args.depth),
        ('base_channels', args.base_channels),
        ('input_shape', (1, 3, 112, 112)),
        ('n_classes', 2),
    ])

    run_config = OrderedDict([
        ('seed', args.seed),
        ('weights', args.weights),
        ('test', args.test),
        ('result', args.result)
    ])

    config = OrderedDict([
        ('model_config', model_config),
        ('run_config', run_config)
    ])

    return config


def load_model(config):
    module = importlib.import_module(config['arch'])
    Network = getattr(module, 'Network')
    return Network(config)

def inference(model, criterion, run_config):
    softmax = nn.Softmax(dim=1)
    model.eval()

    images = run_config['test']
    images = glob.glob(images + "/*")
    outfile = run_config['result']
    fd = open(outfile, "w")
    score = 0

    for image in tqdm.tqdm(images):
        img = imageio.imread(image)
        maxval_axis, argmax_axis = max(img.shape), np.argmax(img.shape)
        padding = [[0, 0], [0, 0], [0, 0]] 
        padding[1 - argmax_axis] = [abs(maxval_axis - img.shape[1 - argmax_axis])//2,
                                    abs(maxval_axis - img.shape[1 - argmax_axis])//2]
        padding = tuple([tuple(x) for x in padding])
        img = np.pad(img, padding, 'constant', constant_values=0)
        img = cv2.resize(img, (112, 112))
        img = TF.to_tensor(img).float().unsqueeze(0)

        with torch.no_grad():
            outputs = softmax(model(img))
            _, preds = torch.max(outputs, dim=1)
        result = preds.cpu().numpy()[0]
        fd.write(image + "," + str(result) + "\n")
        score = score + result
    print(score/len(images))

def main():
    # parse command line arguments
    config = parse_args()
    run_config = config['run_config']
    weights = run_config['weights']

    # model
    model = load_model(config['model_config'])
    #model.cuda()
    weights_ = torch.load(weights)['state_dict']
    model.load_state_dict(weights_)
    criterion = nn.CrossEntropyLoss(reduction='mean')
    # run test before start training
    inference(model, criterion, run_config)

if __name__ == '__main__':
    main()