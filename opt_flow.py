#!/usr/bin/env python

'''
example to show optical flow

USAGE: opt_flow.py [<video_source>]

Keys:
 1 - toggle HSV flow visualization
 2 - toggle glitch

Keys:
    ESC    - exit
'''

# Python 2/3 compatibility
from __future__ import print_function

import numpy as np
import cv2 as cv
import time

import video

def read_points(filename):
    with open(filename) as ifile:
        out = []
        for line in ifile.readlines():
            a, b = line.split(' ')[0:2]
            a = int(a)
            b = int(b)
            out.append((a, b))

    return out


out_res = (224, 224)
ITER_THRESHOLD = 5

def draw_flow(img, flow, step=16):
    h, w = img.shape[:2]
    y, x = np.mgrid[step/2:h:step, step/2:w:step].reshape(2,-1).astype(int)
    fx, fy = flow[y,x].T
    lines = np.vstack([x, y, x+fx, y+fy]).T.reshape(-1, 2, 2)
    lines = np.int32(lines + 0.5)
    vis = cv.cvtColor(img, cv.COLOR_GRAY2BGR)
    cv.polylines(vis, lines, 0, (0, 255, 0))
    for (x1, y1), (_x2, _y2) in lines:
        cv.circle(vis, (x1, y1), 1, (0, 255, 0), -1)
    return vis


def draw_hsv(flow):
    h, w = flow.shape[:2]
    fx, fy = flow[:,:,0], flow[:,:,1]
    ang = np.arctan2(fy, fx) + np.pi
    v = np.sqrt(fx*fx+fy*fy)
    hsv = np.zeros((h, w, 3), np.uint8)
    hsv[...,0] = ang*(180/np.pi/2)
    hsv[...,1] = 255
    hsv[...,2] = np.minimum(v*4, 255)
    bgr = cv.cvtColor(hsv, cv.COLOR_HSV2BGR)
    return bgr


def warp_flow(img, flow):
    h, w = flow.shape[:2]
    flow = -flow
    flow[:,:,0] += np.arange(w)
    flow[:,:,1] += np.arange(h)[:,np.newaxis]
    res = cv.remap(img, flow, None, cv.INTER_LINEAR)
    return res

def isValidBbox(bbox):
    (x, y, w, h) = bbox
    if w > 100 and h > 100 and w < 900 and h < 680:
        return True
    else:
        return False

def projectMap(img, M):
    global out_res
    warp = cv.warpPerspective(img, M, out_res)
    return warp

def drawpoly(img, p):
    return cv.polylines(img, np.array([p], dtype=np.int32),True,(0,255,255))

def add_flow(flows, flow, n):
    if (len(flows) == n):
        del flows[0]
    flows.append(flow)

def avg_flow(flows):
    n = len(flows)
    out = np.zeros_like(flows[0])
    for f in flows:
        out += f / n
    return out

def main():
    global out_res
    import sys
    try:
        fn = sys.argv[1]
    except IndexError:
        fn = 0

    try:
        lfile = sys.argv[2]
    except IndexError:
        lfile = 'label.txt'

    src = read_points(lfile)
    fsrc = np.array(src, dtype=np.float32)

    dst = np.array(((0, 0), (out_res[0] - 1, 0), out_res, (0, out_res[1] - 1)), dtype=np.float32)

    M = cv.getPerspectiveTransform(fsrc, dst)

    kernel = np.ones((11,11),np.uint8)
    cam = video.create_capture(fn)
    _ret, prev = cam.read()
    prevgray = cv.cvtColor(prev, cv.COLOR_BGR2GRAY)
    prevgray = projectMap(prevgray, M)
    show_hsv = False
    show_glitch = False
    cur_glitch = prev.copy()

    flows = []

    bbox_acc = []
    iter_cnt = 0
    while True:
        time.sleep(1/10)
        _ret, img = cam.read()
        vis = img.copy()
                
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        clean = drawpoly(img, src)
        gray = projectMap(gray, M)

        im = cv.erode(gray, kernel,iterations = 1)
        im = cv.dilate(im, kernel,iterations = 1)
        gray = im
        #im = cv.bilateralFilter(im, 5, 0.5, 0.5)
        flow = cv.calcOpticalFlowFarneback(prevgray, gray, None, 0.5, 5, 15, 3, 5, 1.1, flags=cv.OPTFLOW_FARNEBACK_GAUSSIAN)
        prevgray = gray
        add_flow(flows, flow, 10)
        flow = avg_flow(flows)

        cv.imshow('flow', draw_flow(gray, flow))
        cv.imshow('im', im)
        a1 = np.sum(flow, axis=0)
        a2 = np.sum(a1, axis=0)
        print(np.min(im))
        #quit()
        #cv.imshow('flow', flow)
        cur_glitch = warp_flow(cur_glitch, flow)
        #cv.imshow('glitch', cur_glitch)
        #cv.imshow('flow HSV', draw_hsv(flow))
        cv.imshow('img', clean)
        if show_hsv:
            # cv.imshow('flow HSV', draw_hsv(flow))

            if iter_cnt < ITER_THRESHOLD:
                gray1 = cv.cvtColor(draw_hsv(flow), cv.COLOR_BGR2GRAY)
                thresh = cv.threshold(gray1, 50, 255, cv.THRESH_BINARY)[1]
                thresh = cv.dilate(thresh, None, iterations=2)
                (cnts, _) = cv.findContours(thresh.copy(), cv.RETR_EXTERNAL,cv.CHAIN_APPROX_SIMPLE)
        
                # print (cnts)
                cnts = map(lambda c: cv.boundingRect(c), cnts)
                cnts = list(filter(isValidBbox, cnts))

                if len(cnts) == 1:
                    bbox_acc.append(cnts[0])
                    (x, y, w, h) = cnts[0]
                    cv.rectangle(vis, (x, y), (x + w, y + h), (0, 255, 0), 4)
                    cv.putText(vis,str(time.time()), (x,y), cv.FONT_HERSHEY_SIMPLEX, 1, (0,0,255),1)

                iter_cnt += 1

            elif iter_cnt == ITER_THRESHOLD:
                print ("ITER_THRESHOLD")

                res = list(zip(*bbox_acc))
                x_avg = int(sum(res[0]) / len(res[0]))
                y_avg = int(sum(res[1]) / len(res[1]))
                w_avg = int(sum(res[2]) / len(res[2]))
                h_avg = int(sum(res[3]) / len(res[3]))

                print (x_avg, y_avg, w_avg, h_avg)

                cv.rectangle(vis, (x_avg, y_avg), (x_avg + w_avg, y_avg + h_avg), (0, 255, 0), 4)
                # cv.putText(vis,str(time.time()), (x,y), cv.FONT_HERSHEY_SIMPLEX, 1, (0,0,255),1)

            cv.imshow('Image', vis)

        if show_glitch:
            cur_glitch = warp_flow(cur_glitch, flow)
            cv.imshow('glitch', cur_glitch)

        ch = cv.waitKey(5)
        if ch == 27:
            break
        if ch == ord('1'):
            show_hsv = not show_hsv
            print('HSV flow visualization is', ['off', 'on'][show_hsv])
        if ch == ord('2'):
            show_glitch = not show_glitch
            if show_glitch:
                cur_glitch = img.copy()
            print('glitch is', ['off', 'on'][show_glitch])

    print('Done')


if __name__ == '__main__':
    print(__doc__)
    main()
    cv.destroyAllWindows()
