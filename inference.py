#%%
import numpy as np # linear algebra
import json
from matplotlib import pyplot as plt
from skimage.io import imread
from skimage import color
from skimage.transform import resize
from skimage.feature import hog, canny, blob_log, blob_dog
from sklearn import svm
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report,accuracy_score
from skimage.morphology import closing
from skimage import util 
from joblib import dump, load

from cvmodel import get_feature, preprocess

name = 'neg/11.png'
img = imread(name)

clf = load('model.bin')
label = clf.predict([get_feature(preprocess(img))])
print(label)
# %%
